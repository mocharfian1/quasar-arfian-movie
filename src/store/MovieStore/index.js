let counter = 2

export default {
  state(){
    return {
      title: "",
      counter: counter
    }
  },
  getters: {
    doubleCount: (state) => state.counter * 2,
  },
  actions: {
    submitToStorage(data) {
      let curMovie = []
      curMovie = JSON.parse(localStorage.getItem("data-movie"))
      curMovie.push(data)

      localStorage.setItem("data-movie", JSON.stringify(curMovie))

      return true
    },
  },
}
